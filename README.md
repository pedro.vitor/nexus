# Nexus + Cookiecutter!

## Pré requisitos

-   Imagem docker do [Nexus3]([https://hub.docker.com/r/sonatype/nexus3/](https://hub.docker.com/r/sonatype/nexus3/))
```bash
$ docker  pull  sonatype/nexus3
```
- Pacotes python [cookiecutter]([https://cookiecutter.readthedocs.io/en/latest/index.html](https://cookiecutter.readthedocs.io/en/latest/index.html)) e [twine]([https://twine.readthedocs.io/en/latest/](https://twine.readthedocs.io/en/latest/))

```bash
$ pip install --user cookiecutter twine
```


## [Nexus](https://help.sonatype.com/repomanager3)

É um gerenciador de repositórios, um software desenhado para o armazenamento de **componentes** produzidos por times de desenvolvimento, através de **repositórios**.
https://help.sonatype.com/repomanager3/repository-manager-concepts

### Executando Nexus (docker)

```bash
$ docker  run -d -p 8081:8081 --name  nexus  sonatype/nexus3
```

Verificando se o Nexus já está no ar:

```bash
$ curl 'http://localhost:8081'
```

##### Pegando a senha do user 'admin'

```bash
$ docker exec -it nexus bash -c 'cat nexus-data/admin.password'
```

#### [Configurando os repositórios Pypi](https://help.sonatype.com/repomanager3/formats/pypi-repositories)

- Criar o Blob Storage
- Criar o Repositório Pypi internal (pypi-internal)
- Criar o Repositório Pypi group (pypi-all)


#### Configurando o cliente para enviar pacotes para o Nexus

Criar o arquivo `~/.pypirc`:

```bash
$ touch ~/.pypirc
```

Editar o arquivo ` ~/.pypirc`:

```vim
[distutils]
index-servers =
	pypi
	internal
[pypi]
username: 
password: 
[internal]
repository: http://localhost:8081/repository/pypi-internal/
username: admin
password: admin
```


## [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/index.html)

Um utilitário de linha de comando que cria projetos a partir de um template.

### Python package template

- [template_python_package](https://gitlab.com/globo-data-analytics/template_python_package)

### Usando o template

```bash
$ cookiecutter https://gitlab.com/globo-data-analytics/template_python_package
```

### Publicando um pacote python para o Nexus

#### Realizando o Build do projeto

```bash
$ python3 setup.py sdist bdist_wheel
```

#### Fazendo o Upload para o Nexus

```bash
$ twine upload -r internal dist/*
```

### Realizando a instalação do pacote através do Nexus

```bash
pip install python_test_lib --index-url http://localhost:8081/repository/pypi-all/simple/
```

### Instalação usando um Pipfile

```bash
[[source]]
name = "pypi"
url = "https://pypi.org/simple"
verify_ssl = true

[[source]]
name = "internal"
url = "http://localhost:8081/repository/pypi-all/simple/"
verify_ssl = false
```